#!/usr/bin/python3
import random
import multiprocessing
import numpy as np
import matplotlib.pyplot as plt

np.random.seed(354234)


def worker(data, shared_list):
    hist, bin_edges = np.histogram(data)
    shared_list.append(hist)
    return


if __name__ == '__main__':
    manager = multiprocessing.Manager()
    shared_list = manager.list()
    data = np.random.beta(1, 100, 1000000)
    jobs = []
    for i in range(1, 11):
        job = multiprocessing.Process(target=worker, args=[data[(i-1)*100000:(i*100000)-1], shared_list])
        jobs.append(job)
        job.start()

    for job in jobs:
        job.join()

    sum = np.zeros((1, 10))
    for np_list in shared_list:
        sum += np_list

    edges = [0., 9.9, 19.8, 29.7, 39.6, 49.5, 59.4, 69.3, 79.2, 89.1, 99.]
    fig, ax = plt.subplots()
    ax.bar(edges[:-1], sum[0], width=np.diff(edges), edgecolor="black", align="edge")
    plt.show()


