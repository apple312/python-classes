#!/usr/bin/python3
from time import sleep

code = input("Please enter code:")
while True:
    print("Hello do you want to open safe [1]? Or do you want to change the code[2]?")
    c = input("Please enter 1 or 2: ")
    if c == '1':
        given_code = input("Please enter current code:")
        if given_code == code:
            print("*You opened safe, and there is 100$*")
            sleep(2)
        else:
            print("Wrong code, you are blocked for 10s.")
            sleep(10)
    elif c == '2':
        given_code = input("Please enter current code:")
        if given_code == code:
            code = input("Please enter new code:")
            print("You changed code successfully!")
            sleep(2)
        else:
            print("Wrong code, you are blocked for 10s.")
            sleep(10)
    else:
        print("Wrong option! Retry")

