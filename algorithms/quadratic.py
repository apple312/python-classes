#!/usr/bin/python3
import sys
from math import sqrt


def calculate(argv):
    a = float(argv[1])
    b = float(argv[2])
    c = float(argv[3])

    delta = b ** 2 - 4 * a * c
    if delta < 0:
        print("No solutions! Delta < 0")
        return
    elif delta == 0:
        x = -b / (2 * a)
        print("Only one solution. x = ", x)
        return x
    else:
        x1 = -b - sqrt(delta) / (2 * a)
        x2 = -b + sqrt(delta) / (2 * a)
        print("Two solutions x1 = {}, x2 = {}".format(x1,x2))
        return [x1, x2]


if __name__ == "__main__":
    calculate(sys.argv)