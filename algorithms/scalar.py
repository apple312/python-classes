#!/usr/bin/python3

a = [1, 2, 12, 4]
b = [2, 4, 2, 8]
sum = 0
for i in range(len(a)):
    sum += a[i] * b[i]

print("Scalar product of {} and {} is equal to {}".format(a, b, sum))