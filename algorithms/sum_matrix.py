#!/usr/bin/python3
import random


def gen_rand_matrix(size):
    return [[random.random() for j in range(size)] for i in range(size)]


size = 128
A = gen_rand_matrix(size)
B = gen_rand_matrix(size)
# create empty matrix for result
C = [[0 for j in range(size)] for i in range(size)]

for i in range(size):
    for j in range(size):
        C[i][j] = A[i][j] + B[i][j]

print("Result: ", C)
