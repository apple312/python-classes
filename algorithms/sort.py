#!/usr/bin/python3
import random

random_list = random.sample(range(1, 1000), 50)
random_list_sorted = random_list.copy()
list_len = len(random_list)

# it will perform bubble sort, with small optimization
for i in range(list_len - 1):
    for j in range(0, list_len - i - 1):
        if random_list_sorted[j] < random_list_sorted[j + 1]:
            random_list_sorted[j], random_list_sorted[j + 1] = random_list_sorted[j + 1], random_list_sorted[j]

random_list_sorted_python = random_list.copy()
random_list_sorted_python.sort(reverse=True)

print("Not sorted list: ", random_list)
print("List sorted by script: ", random_list_sorted)
print("List sorted by python function: ", random_list_sorted_python)
if random_list_sorted_python == random_list_sorted:
    print("Both sorted list are the same!")
else:
    print("Sorted lists are not the same!")


