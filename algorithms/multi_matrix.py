#!/usr/bin/python3
import random


def gen_rand_matrix(size):
    return [[random.randint(0, 10) for j in range(size)] for i in range(size)]


size = 2
A = gen_rand_matrix(size)
B = gen_rand_matrix(size)
# create empty matrix for result
C = [[0 for j in range(size)] for i in range(size)]
# A * B = C
for i in range(size):
    for j in range(size):
        for k in range(size):
            C[i][j] += A[i][k] * B[k][j]

print("{} multiplicated by {} is equal to {}".format(A, B, C))
