#!/usr/bin/python3
import random


def gen_rand_matrix(size):
    return [[random.randint(0, 10) for j in range(size)] for i in range(size)]


def determinant(matrix):
    dimension = len(matrix)
    if dimension == 1:
        print("Wrong matrix dimension")
        return
    elif dimension == 2:
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]
    else:
        sum = 0
        for i in range(dimension):
            # copy matrix without first row
            tmp_matrix = matrix.copy()[1:]
            for j in range(len(tmp_matrix)):
                # create submatrix
                tmp_matrix[j] = tmp_matrix[j][0:i] + tmp_matrix[j][i+1:]
            # sum and run recursive call of function
            sum += ((-1) ** (i % 2)) * matrix[0][i] * determinant(tmp_matrix)

    return sum


size = random.randint(2, 10)
matrix = gen_rand_matrix(size)
determinant = determinant(matrix)
print("Determinant of matrix {} is equal to: {}".format(matrix, determinant))
