A World Wide Name (WWN) or World Wide Identifier (WWID) is a unique identifier used in storage technologies including Fibre Channel, Advanced Technology Attachment (ATA) or Serial Attached SCSI (SAS).
A WWN may be employed in a variety of roles, such as a serial number or for addressability; for example, in Fibre Channel networks, a WWN may be used as a WWNN (World Wide Node Name) to identify a switch, or a WWPN (World Wide Port Name) to identify an individual port on a switch. Two WWNs which do not refer to the same thing should always be different even if the two are used in different roles, i.e. a role such as WWPN or WWNN does not define a separate WWN space. The use of burned-in addresses and specification compliance by vendors is relied upon to enforce uniqueness.



Each WWN is an 8 or 16 byte number, the length and format of which is determined by the most significant four bits, which are referred to as an NAA (Network Address Authority.) The remainder of the value is derived from an IEEE OUI (often the term "Company Identifier" is used as a synonym for OUI) and vendor-supplied information. Each format defines a different way to arrange and/or interpret these components. OUIs are used with the U/L and multicast bits zeroed, or sometimes even omitted (and assumed zero.)
The WWN formats include:
"Original" IEEE formats are essentially a two-byte header followed by an embedded MAC-48/EUI-48 address (which contains the OUI.) The first 2 bytes are either hex 10:00 or 2x:xx (where the x's are vendor-specified) followed by the 3-byte OUI and 3 bytes for a vendor-specified serial number. Thus, the difference between NAA 1 format and NAA 2 format is merely the presence of either a zero pad or an extra 3 nibbles of vendor information.
"Registered" IEEE formats dispense with padding and place the OUI immediately after the NAA. The OUI is no longer considered to be part of a MAC-48/EUI-48 address. For NAA 5 format, this leaves 9 contiguous nibbles for a vendor-defined value. This is the same format used by the companion NAA 6 format, the only difference being a 16-byte number space is assumed, rather than an 8-byte number space. This leaves a total of 25 contiguous nibbles for vendor-defined values.
"Mapped EUI-64" formats manage to fit an EUI-64 address into an 8-byte WWN. Since the NAA is mandatory, and takes up a nibble, this represents a four-bit deficit. These four bits are recouped through the following tricks: First, two bits are stolen from the NAA by allocating NAAs 12, 13, 14, and 15 to all refer to the same format. Second, the remaining two bits are recouped by omitting the U/L and multicast bits from the EUI-64's OUI. When reconstructing the embedded EUI-64 value, the U/L and multicast bits are assumed to have carried zero values.



WWN addresses are predominantly represented as colon separated hexadecimal octets, MSB-first, with leading zeros   similar to Ethernet's MAC address. However, there is much variance between vendors.



Linux uses WWN to identify disks by providing symbolic links to the real device entry:

ls -l /dev/disk/by-id/
[ ]
lrwxrwxrwx 1 root root  9 Jul  4 22:00 wwn-0x5002e10000000000 -> ../../sr0
lrwxrwxrwx 1 root root  9 Jul  4 22:00 wwn-0x500277a4100c4e21 -> ../../sda
lrwxrwxrwx 1 root root 10 Jul  4 22:00 wwn-0x500277a4100c4e21-part1 -> ../../sda1
lrwxrwxrwx 1 root root 10 Jul  4 22:00 wwn-0x500277a4100c4e21-part2 -> ../../sda2
lrwxrwxrwx 1 root root 10 Jul  4 22:00 wwn-0x500277a4100c4e21-part3 -> ../../sda3

(There are more entries in this directory which are omitted here)
The target names (sr0, sda) might change when new devices are added to the computer (e.g. sda might become sdb) but the WWN will be the same. That is an advantage when the WWN are used in configuration files and scripts, e.g. /etc/fstab.



OUIs can be queried searching the IEEE organization's Public Manufacturers OUI list.
00:10:86 ATTO Technology
00:60:69 Brocade Communications Systems
00:05:1E Brocade Communications Systems, acquired with Rhapsody Networks
00:60:DF Brocade Communications Systems, acquired with CNT Technologies Corporation
08:00:88 Brocade Communications Systems, acquired with McDATA Corporation. WWIDs begin with 1000.080
00:05:30 Cisco
00:05:73 Cisco
00:05:9b Cisco
00:D3:10 Dell, Inc., for Dell Compellent Storage products
00:01:E8 Dell, Inc., for Dell Force10 Networking Products
00:23:29 DDRdrive LLC, for DDRdrive X1
00:60:16 EMC Corporation, for CLARiiON/VNX
00:60:48 EMC Corporation, for Symmetrix DMX
00:00:97 EMC Corporation, for Symmetrix VMAX
00:01:44 EMC Corporation, for VPLEX
00:00:C9 Emulex
00:60:B0 Hewlett-Packard - Integrity and HP9000 servers. WWIDs begin with 5006.0b0
00:11:0A Hewlett-Packard - ProLiant servers. Formerly Compaq. WWIDs begin with 5001.10a
00:01:FE Hewlett-Packard - EVA disk arrays. Formerly Digital Equipment Corporation. WWIDs begin with 5000.1fe1 or 6000.1fe1
00:17:A4 Hewlett-Packard - MSL tape libraries. Formerly Global Data Services. WWIDs begin with 200x.0017.a4
00:0C:CA HGST, a Western Digital Company
00:60:E8 Hitachi Data Systems
00:50:76 IBM
00:17:38 IBM, formerly XIV.
00:A0:98 NetApp
00:01:55 Promise Technology
00:E0:8B QLogic HBAs, original identifier space
00:1B:32 QLogic HBAs. new identifier space starting to be used in 2007
00:C0:DD QLogic FC switches
00:90:66 QLogic formerly Troika Networks
00:11:75 QLogic formerly PathScale, Inc
00:25:38 Samsung Electronics, for solid-state drives
00:1B:44 SanDisk
00:1E:82 SanDisk
00:0C:50 Seagate Technology
00:A0:B8 Symbios Logic Inc.
00:14:EE Western Digital
14:F0:C5 Xtremio (EMC Corporation)
00:50:CC Xyratex



Advanced Technology Attachment (ATA)
Fibre Channel
Persistent binding
Serial Attached SCSI (SAS)
Switched fabric
World Wide Port Name (WWPN)



IEEE OUI list


