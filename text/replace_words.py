#!/usr/bin/python3
import os

words_to_replace = {'never': 'almost never', 'and': 'moreover', 'World': 'Globe'}


def replace_words(file):
    # open file for reading
    f = open(file, "r")
    # copy content of file to one string variable
    content = f.read()
    f.close()

    for key in words_to_replace:
        content = content.replace(key, words_to_replace[key])

    # open file for writing and write modified text
    f = open(file, "w")
    f.write(content)
    f.close()


path_to_files = "./source_files"
for file in os.listdir(path_to_files):
    replace_words(os.path.join(path_to_files, file))
