#!/usr/bin/python3
import os

# provided text repository does not contain polish texts so set of words to delete is different than in exercise
words_to_delete = ['war', 'War', 'China', 'and', 'for']


def delete_words(file):
    # open file for reading
    f = open(file, "r")
    # copy content of file to one string variable
    content = f.read()
    f.close()
    for word in words_to_delete:
        # find word and delete it by replacing with empty string
        content = content.replace(word, "")

    # open file for writing and write modified text
    f = open(file, "w")
    f.write(content)
    f.close()


path_to_files = "./source_files"
for file in os.listdir(path_to_files):
    delete_words(os.path.join(path_to_files, file))