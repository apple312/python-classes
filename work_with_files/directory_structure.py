#!/usr/bin/python3
import os

path_to_explore = "/home/pi/thesis"
for current, dirs, files in os.walk(path_to_explore):
    print("Current:", current)
    print("Subdirs:", dirs)
    print("Files:", files)
    print("-------------")