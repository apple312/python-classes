#!/usr/bin/python3
import os

path_with_images = "."
list = os.listdir(path_with_images)

for file in list:
    if ".jpg" in file:
        os.rename(file, file.replace(".jpg", ".png"))
