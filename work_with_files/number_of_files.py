#!/usr/bin/python3
import os

# create list with all content of dir
content = os.listdir("/dev")
# this will print number of files and directories in /dev
print(len(content), " files and directories are in /dev directory.")

# count and print number of files in /dev
sum = 0
for file in content:
    if os.path.isfile("/dev" + file):
        sum += 1
print(sum, " files are in /dev directory.")