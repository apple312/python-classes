#!/usr/bin/python3
import csv


# open file and convert it to dictionary
def csv_to_list(file_path):
    f = open(file_path)
    records_list = list(csv.reader(f, delimiter=';'))
    f.close()
    return records_list


def print_csv_list(records_list):
    for row in records_list:
        print('\t\t'.join(row))


def delete_record(record_list):
    while True:
        id = input("Provide record id you want to delete: ")
        for record in record_list:
            if record[0] == id:
                record_list.remove(record)
                return
        print("You provided wrong id, cant find it. Try again.")


def add_record(records_list):
    print("Provide new record values.")
    new_record = []
    id = None
    while True:
        id = input("Unique ID: ")
        # need to check if ID is unique
        flag = True
        for record in records_list:
            if record[0] == id:
                print("You need to provide unique ID")
                flag = False
        if flag:
            new_record.append(id)
            break

    name = input("Name: ")
    surname = input("Surname: ")
    test1 = input("Test1 grade: ")
    test2 = input("Test2 grade: ")
    records_list.append([id, name, surname, test1, test2])


def save_csv(file_path, record_list):
    f = open(file_path, 'w')
    writer = csv.writer(f)
    writer.writerows(record_list)


if __name__ == "__main__":
    path_to_file = "./grades.csv"
    csv_list = csv_to_list(path_to_file)
    print("Your data: ")
    print_csv_list(csv_list)

    while True:
        print("You can delete record by ID[1] or add new one[2] or quit and save[3]")
        option = input("Choose option: ")
        if option == '1':
            delete_record(csv_list)
        elif option == '2':
            add_record(csv_list)
        elif option == '3':
            break
        else:
            print("Wrong option!")

    save_csv(path_to_file, csv_list)


