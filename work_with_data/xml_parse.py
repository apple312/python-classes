#!/usr/bin/python3
from xml.dom import minidom

path_to_file = "./test.xml"


def parse_with_dom(file):
    tree = minidom.parse(file)
    breakfast_menu = tree.documentElement
    food_list = breakfast_menu.getElementsByTagName('food')
    for food in food_list:
        if food.getElementsByTagName('name')[0].childNodes[0].data == 'Strawberry Belgian Waffles':
            food.getElementsByTagName('price')[0].childNodes[0].data = "$5"

    f = open('./test_dom_out.xml', 'w')
    f.write(tree.toxml())
    f.close()


parse_with_dom(path_to_file)


