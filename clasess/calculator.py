#!/usr/bin/python3
from complex_numbers import Complex


# 0 for first complex, 1 for second complex number
def parse_num(eq_list, order):
    if eq_list[1 + 4 * order] == '+':
        complex1 = Complex(float(eq_list[0 + 4 * order].replace('(', '')),
                           float(eq_list[2 + 4 * order].replace('i)', '')))
    elif eq_list[1 + 4 * order] == '-':
        complex1 = Complex(float(eq_list[0 + 4 * order].replace('(', '')),
                           -float(eq_list[2 + 4 * order].replace('i)', '')))
    else:
        print("ERROR, bad input format for number ", 1 + order)
        return None
    return complex1


while True:
    print("Hello, I will calculate complex numbers equation!")
    print("You should use i as imaginary unit")
    print("Only +, -, are allowed. Obligatory format is - [a +/- bi +/-/* c +/- di)")
    eq = input("Your equation: ")
    eq_list = eq.split()
    print(eq_list)
    # parse input
    c1 = parse_num(eq_list, 0)
    c2 = parse_num(eq_list, 1)

    if not c1 or not c2:
        continue

    if eq_list[3] == "+":
        Complex.add(c1, c2).print_c()
    elif eq_list[3] == "-":
        Complex.diff(c1, c2).print_c()
    elif eq_list[3] == "*":
        Complex.multiplicate(c1, c2).print_c()
    else:
        print("ERROR, bad input format")
        continue
