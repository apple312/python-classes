#!/usr/bin/python3
from math import sqrt


class Complex:
    def __init__(self, real, imag):
        self.r = real
        self.i = imag

    def print_c(self):
        if self.i >= 0:
            print("{} + {}i".format(self.r, self.i))
        else:
            print("{} - {}i".format(self.r, -self.i))

    def add(self, number):
        return Complex(self.r + number.r, self.i + number.i)

    def sum(number1, number2):
        return Complex(number1.r + number2.r, number1.r + number2.r)

    def subtract(self, number):
        return Complex(self.r - number.r, self.i - number.i)

    def diff(number1, number2):
        return Complex(number1.r - number2.r, number1.i - number2.i)

    def abs(self):
        return sqrt(self.r ** 2, self.i ** 2)

    def multiplicate(number1, number2):
        return Complex(number1.r * number2.r - number1.i * number2.i,
                       number1.r * number2.i + number1.i * number2.r)
